// External
/// <reference path="./../node_modules/@types/jasmine/index.d.ts" />
/// <reference path="./../node_modules/@types/jasmine-jquery/index.d.ts" />

// Testrunner
/// <reference path="./../node_modules/webdriver-client-test-runner/src/webdriver-client-test-runner/typedefs/exports.d.ts" />

let config = require('../configuration/main.config.js');
let webDriverIO = require("webdriverio");

let debugMode: boolean = process.argv.indexOf("--debug") !== -1;

module iviLoadTest {
    const EXIST_TIMEOUT: number = 5000;

    enum eventsTypes {
        adStarted = <any>"adStarted",
        adCompleted = <any>"adCompleted",
        noAd = <any>"noAd",
    }

    let eventsCounter: Object = {
        [eventsTypes.adStarted]: 0,
        [eventsTypes.adCompleted]: 0,
        [eventsTypes.noAd]: 0
    }

    async function initPage() {
        await browser
            .url(config.url)
            .waitForExist(`#vn-player`, EXIST_TIMEOUT)
            .scroll(`#vn-player`)
    }

    async function checkVNPlayerIFrame() {
        await browser.waitForExist(`iframe.___iframe___`, EXIST_TIMEOUT);
        let playerIFrameEl: WebdriverIO.Element =
            (await browser.element(`iframe.___iframe___`)).value;

        await browser
            .frame(playerIFrameEl)
            .waitForExist(`.___videoContainer___`, EXIST_TIMEOUT)
    }

    async function checkIVIPlayerIFrame() {
        await browser.waitForExist(`.___videoContainer___ iframe`, EXIST_TIMEOUT);
        let playerIFrameEl: WebdriverIO.Element =
            (await browser.element(`.___videoContainer___ iframe`)).value;

        await browser
            .frame(playerIFrameEl)
            .waitForExist(`body`, EXIST_TIMEOUT)
    }

    async function returnToWinContext() {
        await browser
            .frameParent()
            .frameParent();
    }

    async function processingIVIEvents() {
        return new Promise((resolve) => {
            browser
                .scroll(`.videonow`)
                .scroll(`#vn-player`)
                .executeAsync((eventsCounter, eventsTypes, done) => {
                    const vpaid = (<any>window).getVPAIDAd();
                    const { iframe } = vpaid._controlData;
                    const frameWin = iframe.node.contentWindow.window;

                    function processingIVIMessages({ data }) {
                        switch (data.message) {
                            case 'ivi-advStarted':
                                console.log('event appeared in VN TEST: ivi-advStarted');
                                ++eventsCounter[eventsTypes.adStarted];
                                break;
                            case 'ivi-advShowed':
                                frameWin.removeEventListener('message', processingIVIMessages);
                                console.log('event appeared in VN TEST: ivi-advShowed');
                                ++eventsCounter[eventsTypes.adCompleted];
                                done(eventsCounter);
                                break;
                            case 'ivi-noAdv':
                                frameWin.removeEventListener('message', processingIVIMessages);
                                console.log('event appeared in VN TEST: ivi-noAdv');
                                ++eventsCounter[eventsTypes.noAd];
                                done(eventsCounter);
                                break;
                        }
                    }

                    frameWin.addEventListener('message', processingIVIMessages, false);
                }, eventsCounter, eventsTypes)
                .then((result) => {
                    eventsCounter = result.value;
                    resolve();
                });
        });
    }

    describe("IVI vpaid load test",  () => {
        for (let i = 1; i < config.loopCount + 1; i++) {
            it(`Test #${i}`, async (done) => {
                browser
                    .timeouts("script", 60000)
                    .timeouts("implicit", 1000)
                    .timeouts("page load", 60000);

                try {
                    await initPage();
                    await checkVNPlayerIFrame();
                    await checkIVIPlayerIFrame();
                    await returnToWinContext();
                    await processingIVIEvents();

                    browser.call(done);
                } catch(err) {
                    if (debugMode) {
                        console.error(err.message);
                    }
                }
            });
        }

        afterAll(() => {
            console.log('\r\n');
            Object
                .keys(eventsCounter)
                .forEach((type) => {
                    console.log(` ---> EVENT: ${type} = ${eventsCounter[type]}`)
                });
        })
    })

}